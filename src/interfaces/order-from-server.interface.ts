import { ItemOrder } from './item-order.interface';

export interface OrderFromServer {
    username: string;
    email: string;
    address: string;
    orderId: string;
    date: Date;
    price: number;
    order: ItemOrder[];
    _isCompleted: boolean;
}