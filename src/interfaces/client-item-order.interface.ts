/** The client sends an Order of Items of the form:
    token: "eyJhbGciOiJSUzI1Ni.IsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZS",
    date: "02-08-2021 14:52:30",
    address: "1234 South 1st Street, City, MI 12345"
    items: [
        {
            id: "127057",
            quantity: 30
        },
        ...
 */
        export interface ClientItemOrder {
            token: string;
            date: Date;
            address: string;
            items: ClientItemOrderData[];
        }
        
        export interface ClientItemOrderData {
            id: string;
            quantity: number;
        }