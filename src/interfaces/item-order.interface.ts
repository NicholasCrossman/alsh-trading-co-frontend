import { Item } from "./item.interface";

export interface ItemOrder{
    item: Item;
    quantity: number;
    totalPrice: number;
}