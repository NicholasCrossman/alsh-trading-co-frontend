export interface Item {
    id: string;
    height: string;
    itemName: string;
    itemType: string;
    price: number;
    imageSrc: string;
}