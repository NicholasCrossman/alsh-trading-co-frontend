import { Component, OnInit } from '@angular/core';
import { OrderFromServer } from 'src/interfaces/order-from-server.interface';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-admin-account-page',
  templateUrl: './admin-account-page.component.html',
  styleUrls: ['./admin-account-page.component.scss']
})
export class AdminAccountPageComponent implements OnInit {
  completedOrders: OrderFromServer[] = [];
  inProgressOrders: OrderFromServer[] = [];

  constructor(public crudService: CrudService) { }

  ngOnInit(): void {
    this.getOrders();
  }

  getOrders(){
    this.completedOrders = [];
    this.inProgressOrders = [];
    this.crudService.getAllOrders().subscribe(data => {
      //console.log("Data: " + JSON.stringify(data));
      let myOrders = JSON.parse(data.body.orders) as OrderFromServer[];
      //console.log(`All Orders Recieved: ${JSON.stringify(myOrders)}`);
      // separate the completed Orders from the in progress Orders
      for(let i = 0; i < myOrders.length ; i++) {
        let nextOrder = myOrders[i];
        if(nextOrder._isCompleted) {
          this.completedOrders.push(nextOrder);
        }
        else {
          this.inProgressOrders.push(nextOrder);
        }
      }
      // sort the orders so the most recent orders appear first
      if(this.inProgressOrders.length > 1) {
        this.inProgressOrders.sort((a, b) => {
          let first = new Date(a.date);
          let second = new Date(b.date);
          return second.getMilliseconds() - first.getMilliseconds();
        });
      }
      if(this.completedOrders.length > 1) {
        this.completedOrders.sort((a, b) => {
          let first = new Date(a.date);
          let second = new Date(b.date);
          return second.getMilliseconds() - first.getMilliseconds();
        });
      }
    })
  }

  completeOrder(orderId: string) {
    this.crudService.completeOrder(orderId).subscribe(data => {
      console.log(data);
      this.getOrders();
    })
  }

  displayDate(input: Date): string {
    let date = new Date(input);
    return date.toLocaleDateString("en-US");
  }

  displayTime(input: Date): string {
    let date = new Date(input);
    return date.toLocaleTimeString("en-US");
  }

  displayDropdown(i: string) {
    document.getElementById(i).classList.toggle("show");
  }

}
