import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService } from '../crud.service';
import { NbToastrService } from '@nebular/theme';

@Component({
  selector: 'app-login-form', 
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private fb: FormBuilder, public crudService: CrudService, private router: Router, 
    private toastrService: NbToastrService) { }

  ngOnInit(): void {

    this.loginForm = this.fb.group({
      username: ['', [
        Validators.required
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(5)
      ]]
    })
  }

  get username() {
    return this.loginForm.get('username');
  }

  get password() {
    return this.loginForm.get('password');
  }

  showToast(title: string, message: string) {
    this.toastrService.show(
      message,
      title);
  }

  onSubmit() {
    this.crudService.login(this.username.value, this.password.value).subscribe((data: HttpResponse<any>) => {
      console.log(`Data recieved: ${JSON.stringify(data)}`);
      if(data.status == 200){
        console.log("Login success!")
        this.router.navigate(['/items']);
      }
    }, (error: any) => {
      console.log("Login failed. Showing toast.")
      // the login was incorrect. Display a message to the user.
      this.showToast("Login failed. Username or password incorrect.",
        "Login Error");
    });
  }

}
