import { Injectable, EventEmitter } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subscription, throwError } from 'rxjs';
import { retry, catchError, tap} from 'rxjs/operators';
import { Item } from '../interfaces/item.interface';
import { Subject } from 'rxjs';
import { ClientItemOrder, ClientItemOrderData } from 'src/interfaces/client-item-order.interface';
import { OrderFromServer } from 'src/interfaces/order-from-server.interface';
@Injectable({
  providedIn: 'root'
})
export class CrudService {

  endPoint = 'http://127.0.0.1:3000';
  private token: string;
  EmitLogin = new EventEmitter<any>();
  isAdmin: boolean;
  isLoggedIn: boolean;
  username: string;

  constructor(private httpClient: HttpClient) {
    this.token = "";
  }

  setToken(token: string) {
    this.token = token;
    console.log(`Token set: ${this.token}`);
  }

  login(username: string, password: string): Observable<any> {
    console.log(`Outgoing data: user: ${username}, password: ${password}`);
    let data = {
      username: username,
      password: password
    }
    return this.httpClient.post<any>(this.endPoint + '/login', data, {observe: 'response'})
      .pipe(
        tap(data => {
          let tokenData = data.body.token;
          this.isLoggedIn = true;
          this.isAdmin = data.body.isAdmin;
          this.username = data.body.username;
          this.setToken(tokenData);
          this.EmitLogin.emit();
        }),
        catchError(this.httpError)
      )
  }

  register(username: string, email: string, password: string): Observable<any> {
    let data = {
      username: username,
      email: email,
      password: password
    }
    return this.httpClient.post<any>(this.endPoint + '/register', data, {observe: 'response'})
      .pipe(
        catchError(this.httpError)
      )
  }

  logout() {
    this.token = "";
    this.isLoggedIn = false;
    this.isAdmin = false;
    this.EmitLogin.emit();
  }

  getUserData(username: string): Observable<any> {
    return this.httpClient.get<any>(this.endPoint + '/users/' + username, {observe: 'response'})
      .pipe(
        catchError(this.httpError)
      )
  }

  // the following methods deal with Items

  getItems(): Observable<Item> {
    return this.httpClient.get<Item>(this.endPoint + '/items')
      .pipe(
        retry(1),
        catchError(this.httpError)
      )
  }

  getItem(id: string): Observable<Item> {
    return this.httpClient.get<Item>(this.endPoint + '/items/' + id)
      .pipe(
        retry(1),
        catchError(this.httpError)
      )
  }

  createItem(item: FormData): Observable<any> {
    console.log("Outgoing data: " + JSON.stringify(item.get('item-name')));
    // add the Admin's JWT token to authorize the request
    let httpHeader = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      })
    }    
    return this.httpClient.post<any>(this.endPoint + '/items', item, httpHeader)
      .pipe(
        catchError(this.httpError)
      )
  }

  deleteItem(id: string): Observable<any> {
    // add the Admin's JWT token to authorize the request
    let httpHeader = {
      headers: new HttpHeaders({
        'Authorization': `Bearer ${this.token}`
      })
    }    
    return this.httpClient.delete<any>(this.endPoint + '/items/' + id, httpHeader)
      .pipe(
        catchError(this.httpError)
      )
  }

  // Orders methods

  sendOrder(items: ClientItemOrderData[], address: string): Observable<any> {
    let date: Date = new Date();
    let order: ClientItemOrder = {
      token: this.token,
      date: date,
      address: address,
      items: items
    }
    console.log(`Outgoing order: ${JSON.stringify(order)}`);
    return this.httpClient.post<any>(this.endPoint + '/orders', order, {observe: 'response'})
      .pipe(
        catchError(this.httpError)
      )
  }

  getAllOrders(): Observable<any> {
    return this.httpClient.get<any>(this.endPoint + '/orders', {observe: 'response'})
      .pipe(
        catchError(this.httpError)
      )
  }

  getOrdersByEmail(email: string) : Observable<any> {
    return this.httpClient.get<OrderFromServer>(this.endPoint + '/users/' + email + '/order', {observe: 'response'})
      .pipe(
        catchError(this.httpError)
      )
  }

  completeOrder(orderId: string): Observable<any> {
    return this.httpClient.post<any>(this.endPoint + '/orders/complete/' + orderId, {observe: 'response'})
      .pipe(
        catchError(this.httpError)
      )
  }

  httpError(error: any) {
    let msg = '';
    if(error.error instanceof ErrorEvent) {
      // the error is client-side
      msg = error.error.message;
    } else {
      // the error is on the server
      msg = `Server Error Code: ${error.status}\nMessage: ${error.message}`;
    }
    console.log(msg);
    return throwError(msg);
  }
}
