import { Component, OnInit } from '@angular/core';
import { NbDialogService, NbSidebarService, NbToastrService } from '@nebular/theme';
import { CrudService } from '../crud.service';
import { ItemQuantityPromptComponent } from '../item-quantity-prompt/item-quantity-prompt.component';
import { Item } from '../../interfaces/item.interface';
import { ItemOrder } from '../../interfaces/item-order.interface';
import { SubmitOrderFormComponent } from '../submit-order-form/submit-order-form.component';
import { ClientItemOrderData } from 'src/interfaces/client-item-order.interface';

@Component({
  selector: 'app-item-list',
  templateUrl: './item-list.component.html',
  styleUrls: ['./item-list.component.scss']
})
export class ItemListComponent implements OnInit {
  Items: Item[] = [];
  itemOrders: ItemOrder[] = [];
  isLoggedIn: boolean = false;
  isAdmin: boolean = false;
  username: string = "";
  itemAdded: Event;

  constructor(public crudService: CrudService, private toastrService: NbToastrService,
    private dialogService: NbDialogService,
    private sidebarService: NbSidebarService) {
    this.crudService.EmitLogin.subscribe(() => {
      this.isLoggedIn = this.crudService.isLoggedIn;
      this.isAdmin = this.crudService.isAdmin;
      this.username = this.crudService.username;
    })
  }

  ngOnInit(): void {
    this.sidebarService.collapse();
    this.getItems();
    this.isAdmin = this.crudService.isAdmin;
    this.isLoggedIn = this.crudService.isLoggedIn;
  }

  logout() {
    this.sidebarService.collapse();
    this.isLoggedIn = false;
    this.isAdmin = false;
    this.crudService.logout();
  }

  getItems() {
    return this.crudService.getItems().subscribe((data: {}) => {
      let itemData = JSON.parse(data as string);
      console.log("Items: " + data);
      this.Items = itemData;
    });
  }

  removeItem(id: string) {
    this.crudService.deleteItem(id).subscribe((data) => {
      this.Items = this.Items.filter(value => value.id !== id);
      this.showToast('Item Deleted.', `Item: ${data}`);
    })
  }

  // below are the methods for dealing with Item Orders

  itemAddedToOrder(itemOrder: ItemOrder) {
    this.sidebarService.expand();
    this.dialogService.open(ItemQuantityPromptComponent, {context: {
      itemOrder: itemOrder
    }})
      .onClose.subscribe((quantity) => {
        if(quantity) {
          itemOrder.quantity = quantity;
          itemOrder.totalPrice = itemOrder.item.price * itemOrder.quantity;
          this.itemOrders.push(itemOrder);
          this.showToast('Item added to order', `${itemOrder.quantity} ${itemOrder.item.itemName}`);
        }
      })
  }

  removeFromOrder(id: string) {
    this.itemOrders = this.itemOrders.filter(value => value.item.id !== id);
    this.showToast('Item removed', 'from your order');
  }

  orderTotal(): number {
    let total = 0;
    for(let i=0; i < this.itemOrders.length; i++) {
      let order: ItemOrder = this.itemOrders[i];
      total += order.quantity * order.item.price;
    }
    return total;
  }

  submitOrder() {
    this.dialogService.open(SubmitOrderFormComponent, {context: {
      total: this.orderTotal(),
      items: this.itemOrders
    }})
      .onClose.subscribe(address => {
        if(address) {
          let items: ClientItemOrderData[] = [];
          // turn the list of ItemOrder objects into the correct form
          for(let i=0; i < this.itemOrders.length; i++) {
            let order: ItemOrder = this.itemOrders[i];
            let item: ClientItemOrderData = {
              id: order.item.id,
              quantity: order.quantity
            }
            items.push(item);
          }

          this.crudService.sendOrder(items, address).subscribe(data => {
            this.showToast('Order submitted.', `Order: ${data}`);
            this.itemOrders = [];
            this.crudService.getAllOrders().subscribe(data => {
              console.log(data);
            })
          })
        }
      })
  }

  showToast(title: string, message: string) {
    this.toastrService.show(
      message,
      title);
  }

}
