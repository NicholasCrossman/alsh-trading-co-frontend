import { Component, Input, Output, EventEmitter } from '@angular/core';
import { CrudService } from '../crud.service';
import { ItemOrder } from '../../interfaces/item-order.interface';
import { Item } from '../../interfaces/item.interface';

@Component({
  selector: 'app-item',
  templateUrl: `./item.component.html`,
  styleUrls: ['./item.component.scss']
})
export class ItemComponent {
  
  @Input('item') item: any;
  @Input('isAdmin') isAdmin: any;
  @Input('isLoggedIn') isLoggedIn: any;

  @Output('remove-item') removeItem: EventEmitter<string> = new EventEmitter<string>();
  @Output('add-to-order') addItemToOrder: EventEmitter<ItemOrder> = new EventEmitter<ItemOrder>();

  constructor(public crudService: CrudService) { }

  ngOnInit(): void {
  }

  addToOrder() {
    let itemOrder: ItemOrder = {
      item: this.item,
      quantity: 0,
      totalPrice: 0
    }
    this.addItemToOrder.emit(itemOrder);
  }

  delete() {
    this.removeItem.emit(this.item.id);
  }

}
