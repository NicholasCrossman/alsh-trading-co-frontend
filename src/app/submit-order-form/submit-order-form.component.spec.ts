import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SubmitOrderFormComponent } from './submit-order-form.component';

describe('SubmitOrderFormComponent', () => {
  let component: SubmitOrderFormComponent;
  let fixture: ComponentFixture<SubmitOrderFormComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SubmitOrderFormComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SubmitOrderFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
