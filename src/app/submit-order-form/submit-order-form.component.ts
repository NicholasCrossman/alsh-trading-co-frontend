import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogRef } from '@nebular/theme';

@Component({
  selector: 'app-submit-order-form',
  templateUrl: './submit-order-form.component.html',
  styleUrls: ['./submit-order-form.component.scss']
})
export class SubmitOrderFormComponent implements OnInit {

  @Input('total') total: any;
  @Input('items') items: any;
  submitOrderForm: FormGroup;
  
  constructor(private fb: FormBuilder, protected dialogRef: NbDialogRef<SubmitOrderFormComponent>) { }

  ngOnInit(): void {
    this.submitOrderForm = this.fb.group({
      address: ['', [
        Validators.required,
        Validators.minLength(8)
      ]]
    })
  }

  get address() {
    return this.submitOrderForm.get('address');
  }

  close() {
    this.dialogRef.close();
  }

  onSubmit() {
    this.dialogRef.close(this.address.value);
  }
}
