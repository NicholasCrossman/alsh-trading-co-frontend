import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ItemComponent } from './item/item.component';
import { ItemListComponent } from './item-list/item-list.component';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { NbThemeModule, NbLayoutModule, NbButtonModule, NbSidebarModule, 
  NbCardModule, NbAccordionModule, NbToastrModule, NbDialogModule, NbIconModule } from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';

import {HttpClientModule} from '@angular/common/http';

import { ReactiveFormsModule } from '@angular/forms';
import { NbInputModule, NbSelectModule, NbCheckboxModule, NbButtonGroupModule } from '@nebular/theme';
import { CreateItemFormComponent } from './create-item-form/create-item-form.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { ItemQuantityPromptComponent } from './item-quantity-prompt/item-quantity-prompt.component';
import { NewUserFormComponent } from './new-user-form/new-user-form.component';
import { SubmitOrderFormComponent } from './submit-order-form/submit-order-form.component';
import { ClientAccountPageComponent } from './client-account-page/client-account-page.component';
import { AdminAccountPageComponent } from './admin-account-page/admin-account-page.component';

@NgModule({
  declarations: [
    AppComponent,
    ItemComponent,
    ItemListComponent,
    CreateItemFormComponent,
    LoginFormComponent,
    ItemQuantityPromptComponent,
    NewUserFormComponent,
    SubmitOrderFormComponent,
    ClientAccountPageComponent,
    AdminAccountPageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NoopAnimationsModule,
    NbThemeModule.forRoot({ name: 'dark' }),
    NbToastrModule.forRoot(),
    NbDialogModule.forRoot(),
    NbLayoutModule,
    NbEvaIconsModule,
    NbButtonModule,
    NbSidebarModule.forRoot(),
    NbCardModule,
    NbAccordionModule,
    ReactiveFormsModule,
    NbInputModule,
    NbSelectModule,
    NbCheckboxModule,
    NbButtonGroupModule,
    NbIconModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
