import { HttpResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-new-user-form',
  templateUrl: './new-user-form.component.html',
  styleUrls: ['./new-user-form.component.scss']
})
export class NewUserFormComponent implements OnInit {

  newUserForm: FormGroup;

  constructor(private fb: FormBuilder, public crudService: CrudService, private router: Router) { }

  ngOnInit(): void {
    this.newUserForm = this.fb.group({
      username: ['', [
        Validators.required
      ]],
      email: ['', [
        Validators.email,
        Validators.required
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(5)
      ]]
    })
  }

  get username() {
    return this.newUserForm.get('username');
  }

  get email() {
    return this.newUserForm.get('email');
  }

  get password() {
    return this.newUserForm.get('password');
  }

  onSubmit() {
    this.crudService.register(this.username.value, this.email.value, this.password.value).subscribe((data: HttpResponse<any>) => {
      console.log(`Data recieved: ${JSON.stringify(data)}`);
      if(data.status == 200){
        this.router.navigate(['/login']);
      }
      else if(data.status == 401) {
        console.log('Error: ' + data.body.message);
      }
    })
  }

}
