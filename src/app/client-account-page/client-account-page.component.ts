import { Component, Input, OnInit } from '@angular/core';
import { CrudService } from '../crud.service';
import { OrderFromServer } from 'src/interfaces/order-from-server.interface';

@Component({
  selector: 'app-client-account-page',
  templateUrl: './client-account-page.component.html',
  styleUrls: ['./client-account-page.component.scss']
})
export class ClientAccountPageComponent implements OnInit {

  username: string = "";
  email: string = "";
  //myOrders: OrderFromServer[] = [];
  completedOrders: OrderFromServer[] = [];
  inProgressOrders: OrderFromServer[] = [];

  constructor(public crudService: CrudService) { }

  ngOnInit(): void {
    this.username = this.crudService.username;
    this.getUserData();
  }

  getUserData(){
    this.crudService.getUserData(this.username).subscribe(data => {
      console.log(`User data: ${data.body.user}`);
      let user = JSON.parse(data.body.user);
      this.email = user.email;
      this.getOrders();
    })
  }

  getOrders(){
    this.completedOrders = [];
    this.inProgressOrders = [];
    this.crudService.getOrdersByEmail(this.email).subscribe(data => {
      let myOrders = JSON.parse(data.body.orders) as OrderFromServer[];
      // separate the completed Orders from the in progress Orders
      for(let i = 0; i < myOrders.length ; i++) {
        let nextOrder = myOrders[i];
        if(nextOrder._isCompleted) {
          this.completedOrders.push(nextOrder);
        }
        else {
          this.inProgressOrders.push(nextOrder);
        }
      }
      // sort the orders so the most recent orders appear first
      if(this.inProgressOrders.length > 1) {
        this.inProgressOrders.sort((a, b) => {
          let first = new Date(a.date);
          let second = new Date(b.date);
          return second.getMilliseconds() - first.getMilliseconds();
        });
      }
      if(this.completedOrders.length > 1) {
        this.completedOrders.sort((a, b) => {
          let first = new Date(a.date);
          let second = new Date(b.date);
          return second.getMilliseconds() - first.getMilliseconds();
        });
      }      
      //console.log(`Orders Recieved: ${JSON.stringify(myOrders)}`);
    })
  }

  displayDate(input: Date): string {
    let date = new Date(input);
    return date.toLocaleDateString("en-US");
  }

  displayTime(input: Date): string {
    let date = new Date(input);
    return date.toLocaleTimeString("en-US");
  }

  displayDropdown(i: string) {
    document.getElementById(i).classList.toggle("show");
  }
}
