import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NbDialogRef } from '@nebular/theme';
import { ItemOrder } from '../../interfaces/item-order.interface';

@Component({
  selector: 'app-item-quantity-prompt',
  template: `      
    <div class="cards">
      <app-item [item]="itemOrder.item"></app-item>
      <div class="icon">
        <nb-icon icon="close"></nb-icon>
      </div>
      <nb-card size="small">
        <nb-card-header>Confirm Quantity</nb-card-header>
        <form [formGroup]="quantityForm" (ngSubmit)="onSubmit()">

        <nb-card-body>
          <div class="quantity">
            <label>Quantity: </label>
            <input nbInput size="8" type="text" fieldSize="small" formControlName="quantity">
          </div>
        </nb-card-body>

          <nb-card-footer>
            <button nbButton status="danger" (click)="close()">Cancel</button>
            <input nbButton type="submit" value="Add Items" status="primary"/>
          </nb-card-footer>
        </form>
      </nb-card>
  </div>
  `,
  styles: [`
    .cards {
      display: flex;
      justify-content: center;
      align-items: center;
    }

    .icon {
      display: flex;
      align-items: center;
      background-color:
    }

    .quantity {
      display: flex;
      justify-content: center;
      align-items: center;
      padding: 1em;
    }
  `]
})
export class ItemQuantityPromptComponent implements OnInit {

  itemOrder: ItemOrder;
  quantityForm: FormGroup;

  constructor(private fb: FormBuilder, protected dialogRef: NbDialogRef<ItemQuantityPromptComponent>) { }

  ngOnInit(): void {
    this.quantityForm = this.fb.group({
      quantity: [0, [
        Validators.required,
        Validators.min(1)
      ]]
    })
  }

  get quantity() {
    return this.quantityForm.get('quantity');
  }

  onSubmit() {
    this.dialogRef.close(this.quantity.value);    
  }

  close() {
    this.dialogRef.close();
  }

}
