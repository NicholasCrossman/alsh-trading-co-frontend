import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemQuantityPromptComponent } from './item-quantity-prompt.component';

describe('ItemQuantityPromptComponent', () => {
  let component: ItemQuantityPromptComponent;
  let fixture: ComponentFixture<ItemQuantityPromptComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemQuantityPromptComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemQuantityPromptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
