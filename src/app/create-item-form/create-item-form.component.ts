import { Component, OnInit, } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { CrudService } from '../crud.service';

@Component({
  selector: 'app-create-item-form',
  templateUrl: './create-item-form.component.html',
  styleUrls: ['./create-item-form.component.scss']
})
export class CreateItemFormComponent implements OnInit {

  itemForm: FormGroup;
  photo: File;

  constructor(private fb: FormBuilder, public crudService: CrudService, private router: Router) { 
  }

  ngOnInit(): void {
    this.itemForm = this.fb.group({
      id: ['', [
        Validators.required
      ]],
      height: ['', [
        Validators.required
      ]],
      name: ['', [
        Validators.required
      ]],
      type: ['', Validators.required ],
      price: [0, [
        Validators.required,
        Validators.min(0)
      ]]
    })
  }

  get id() {
    return this.itemForm.get('id');
  }

  get height() {
    return this.itemForm.get('height');
  }

  get name() {
    return this.itemForm.get('name');
  }

  get type() {
    return this.itemForm.get('type');
  }

  get price() {
    return this.itemForm.get('price');
  }

  onChange(event) {
    this.photo = event.target.files[0];
    console.log("File stored.");
  }

  // choose type using select
  changeType(e) {
    this.type.setValue(e.target.value, {
      onlySelf: true
    })
  }

  onSubmit() {
    console.log("Form submitted.");
    // put all values into a FormData object
    let item = new FormData();

    item.append('item-id', this.id.value);
    item.append('item-height', this.height.value);
    item.append('item-name', this.name.value);
    item.append('item-type', this.type.value);
    item.append('price', this.price.value);
    item.append('photo', this.photo, this.photo.name);

    console.log("Original item data: " + JSON.stringify(item.get('item-name')));

    this.crudService.createItem(item).subscribe((data: {}) => {
      console.log("Response: " + JSON.stringify(data));
      this.router.navigate(['/items']);
    });
  }

}
