import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdminAccountPageComponent } from './admin-account-page/admin-account-page.component';
import { ClientAccountPageComponent } from './client-account-page/client-account-page.component';
import { CreateItemFormComponent } from './create-item-form/create-item-form.component';
import { ItemListComponent } from './item-list/item-list.component';
import { LoginFormComponent } from './login-form/login-form.component';
import { NewUserFormComponent } from './new-user-form/new-user-form.component';

const routes: Routes = [
  {path: 'items', component: ItemListComponent},
  {path: '', redirectTo: '/items', pathMatch: 'full'},
  {path: 'login', component: LoginFormComponent},
  {path: 'register', component: NewUserFormComponent},
  {path: 'create-item', component: CreateItemFormComponent},
  {path: 'user', component: ClientAccountPageComponent},
  {path: 'admin', component: AdminAccountPageComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
